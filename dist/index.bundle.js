/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
function createIntroElement() {
    const element = document.createElement('div');

    element.innerHTML = 'Typescript Webpack Template';

    return element;
}

document.body.appendChild(createIntroElement());


/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly90eXBlc2NyaXB0LXdlYnBhY2stdGVtcGxhdGUvLi9zcmMvaW5kZXguanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUEiLCJmaWxlIjoiaW5kZXguYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiZnVuY3Rpb24gY3JlYXRlSW50cm9FbGVtZW50KCkge1xyXG4gICAgY29uc3QgZWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG5cclxuICAgIGVsZW1lbnQuaW5uZXJIVE1MID0gJ1R5cGVzY3JpcHQgV2VicGFjayBUZW1wbGF0ZSc7XHJcblxyXG4gICAgcmV0dXJuIGVsZW1lbnQ7XHJcbn1cclxuXHJcbmRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQoY3JlYXRlSW50cm9FbGVtZW50KCkpO1xyXG5cclxuIl0sInNvdXJjZVJvb3QiOiIifQ==