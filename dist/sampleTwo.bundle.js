/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!*********************************!*\
  !*** ./src/sample-two/index.js ***!
  \*********************************/
function createDivElement() {
    const element = document.createElement('div');

    return element;
}

function createSampleButton() {
    const button = document.createElement('button');

    button.innerHTML = 'Sample Button';

    button.onclick = function () {
        alert('Sample Alert');
    }

    return button;
}

const divElement = createDivElement();
divElement.appendChild(createSampleButton());

document.body.appendChild(divElement);
/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly90eXBlc2NyaXB0LXdlYnBhY2stdGVtcGxhdGUvLi9zcmMvc2FtcGxlLXR3by9pbmRleC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsc0MiLCJmaWxlIjoic2FtcGxlVHdvLmJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImZ1bmN0aW9uIGNyZWF0ZURpdkVsZW1lbnQoKSB7XHJcbiAgICBjb25zdCBlbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XHJcblxyXG4gICAgcmV0dXJuIGVsZW1lbnQ7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGNyZWF0ZVNhbXBsZUJ1dHRvbigpIHtcclxuICAgIGNvbnN0IGJ1dHRvbiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2J1dHRvbicpO1xyXG5cclxuICAgIGJ1dHRvbi5pbm5lckhUTUwgPSAnU2FtcGxlIEJ1dHRvbic7XHJcblxyXG4gICAgYnV0dG9uLm9uY2xpY2sgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgYWxlcnQoJ1NhbXBsZSBBbGVydCcpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBidXR0b247XHJcbn1cclxuXHJcbmNvbnN0IGRpdkVsZW1lbnQgPSBjcmVhdGVEaXZFbGVtZW50KCk7XHJcbmRpdkVsZW1lbnQuYXBwZW5kQ2hpbGQoY3JlYXRlU2FtcGxlQnV0dG9uKCkpO1xyXG5cclxuZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChkaXZFbGVtZW50KTsiXSwic291cmNlUm9vdCI6IiJ9