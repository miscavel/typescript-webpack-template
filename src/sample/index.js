import './style.css';
import cogwheel from './cogwheel.png';

function createSampleElement() {
    const element = document.createElement('div');
    element.classList.add('sample-class');

    element.innerHTML = 'Sample class';

    return element;
}

function createSampleImage() {
    const image = new Image(50, 50);

    image.src = cogwheel;

    return image;
}

document.body.appendChild(createSampleElement());
document.body.appendChild(createSampleImage());