function createDivElement() {
    const element = document.createElement('div');

    return element;
}

function createSampleButton() {
    const button = document.createElement('button');

    button.innerHTML = 'Sample Button';

    button.onclick = function () {
        alert('Sample Alert');
    }

    return button;
}

const divElement = createDivElement();
divElement.appendChild(createSampleButton());

document.body.appendChild(divElement);